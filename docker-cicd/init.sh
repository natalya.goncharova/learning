#! /bin/sh
mkdir ./db/

# User credentials
user=admin
email=admin@example.com
password=pass

file=db/db.sqlite3
#if [ -z "$file" ]; then
#  echo "from django.contrib.auth.models import User; User.objects.create_superuser('$user', '$email', '$password')" | python3 manage.py shell
#fi

python3 manage.py makemigrations
python3 manage.py migrate --run-syncdb  

python3 manage.py createsuperuser
echo "from django.contrib.auth.models import User; User.objects.create_superuser('$user', '$email', '$password')" | python3 manage.py shell
